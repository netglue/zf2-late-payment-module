# UK Late Payment Legislation Interest Calculator

This is a [Zend Framework](http://framework.zend.com) 2 module that provides
services, views, controllers etc for simply calculating debt recovery fees and
late payment interest on commercial debts in the UK.

It also provides services for retreiving the Bank of England base rates and keeping
the rates updated.

You can [see the late payment calculator in action](http://netglue.co/late-payment-interest-calculator) on our website.

## Installation

Install with composer. The package identifier is `"netglue/zf2-late-payment-module"`

Once you've done `php composer.phar update`, add the module name to you app wide config using the name `NetglueLatePayment`

Finally, configure options. Take a look in `src/config/module.config.php` at the defaults. You will need to at least change the storage directory for caching the bank of england base rates.

## Feedback, contrib

Feedback/contributions/corrections are welcome. Please fork, make pull requests, raise issues etc.

## Tests

Tests are not complete though a reasonable amount has been done. I'm having trouble testing the controllers so these aren't covered at all. If you can help with that. Great!


