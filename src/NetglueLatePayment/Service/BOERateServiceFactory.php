<?php
/**
 * A factory for setting up an instance of the BOE Rate Service
 */

namespace NetglueLatePayment\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BOERateServiceFactory implements FactoryInterface {
	
	
	/**
	 * Create service
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return CurrencyConverter|false
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('config');
		$config = isset($config['boe_rates']) ? $config['boe_rates'] : array();
		
		$options = new BOERateServiceOptions($config);
		$service = new BOERateService;
		$service->setOptions($options);
		return $service;
	}
	
}
