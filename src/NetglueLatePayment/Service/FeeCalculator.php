<?php
namespace NetglueLatePayment\Service;

use NetglueLatePayment\Exception;

use DateTime;
use DateInterval;

use Zend\InputFilter\Factory as InputFilterFactory;

class FeeCalculator {
	
	/**
	 * Fixed fees as defined by the legislation
	 * @var array
	 */
	protected $fixedFees = array(
		array(
			'minValue' => 0.01,
			'maxValue' => 999.99,
			'amount' => 40.0,
		),
		array(
			'minValue' => 1000,
			'maxValue' => 9999.99,
			'amount' => 70.0,
		),
		array(
			'minValue' => 10000,
			'maxValue' => NULL,
			'amount' => 100.0,
		),
	);
	
	/**
	 * BoE Rate Service
	 * @var BOERateService|NULL
	 */
	protected $rateService;
	
	/**
	 * Date Format Specifier
	 * @var string
	 */
	protected $dateFormat = 'Y-m-d';
	
	/**
	 * Due Date of Invoice
	 * @var DateTime|NULL
	 */
	protected $dueDate;
	
	/**
	 * Date of Invoice
	 * @var DateTime|NULL
	 */
	protected $invoiceDate;
	
	/**
	 * Terms - Integer number of days
	 * @var int|NULL
	 */
	protected $terms = 30;
	
	/**
	 * Invoice Amount
	 * @var float|int|NULL
	 */
	protected $invoiceAmount;
	
	/**
	 * Statutory Interest Rate
	 * @var float
	 */
	protected $statutoryInterest = 8.0;
	
	/**
	 * Late Payment Fee Chargeable
	 * @var float|int|NULL
	 */
	protected $fee;
	
	/**
	 * Interest Chargeable
	 * @var float|NULL
	 */
	protected $interest;
	
	/**
	 * Number of days overdue
	 * @var int|NULL
	 */
	protected $daysOverdue;
	
	/**
	 * Daily Interest Amount
	 * @var float|NULL
	 */
	protected $dailyInterest;
	
	/**
	 * Interest Rate Used
	 * @var float|NULL
	 */
	protected $interestRate;
	
	/**
	 * Reference Rate Used
	 * @var float|NULL
	 */
	protected $referenceRate;
	
	/**
	 * Total Payable
	 * @var float|NULL
	 */
	protected $grandTotal;
	
	/**
	 * Set the BOE Rate Service Dependency
	 * @param BOERateService $service
	 * @return self
	 */
	public function setRateService(BOERateService $service) {
		$this->rateService = $service;
		return $this;
	}
	
	/**
	 * Return the BOE Rate Service
	 * @return BOERateService|NULL
	 */
	public function getRateService() {
		return $this->rateService;
	}
	
	/**
	 * Return date format
	 * @return string
	 */
	public function getDateFormat() {
		return $this->dateFormat;
	}
	
	/**
	 * Set Date Format
	 * @param string $format
	 * @return self
	 */
	public function setDateFormat($format) {
		$this->dateFormat = (string) $format;
		return $this;
	}
	
	/**
	 * Return the effective reference rate for the given date
	 *
	 * The reference rate is set for 6 months based on the Bank of England Base
	 * Rate as at 31st December, or 30th June. Anything after 1st July will therefore
	 * use the rate from 30th June previous. Any date between 1st Jan and 30th June will
	 * use the rate from 31st December previous.
	 *
	 * @param DateTime|string $date
	 * @return float
	 */
	public function getReferenceRateOnDate($date) {
		if(!$date instanceof DateTime) {
			$date = new DateTime($date);
		}
		$refDate = clone($date);
		if($date->format('n') > 6) {
			$refDate->setDate($date->format('Y'), 6, 30);
		} else {
			$refDate->setDate(($date->format("Y") - 1), 12, 31);
		}
		$rate = $this->getRateService()->getRateOnDate($refDate);
		if(false === $rate) {
			throw new Exception\RuntimeException('It was not possible to retrieve a reference rate for '.$date->format('l jS F Y'));
		}
		return $rate;
	}
	
	/**
	 * Set the Due Date of the invoice
	 * @param string|DateTime $date
	 * @return self
	 */
	public function setDueDate($date) {
		if(!$date instanceof DateTime) {
			$date = new DateTime($date);
		}
		// Reset Affected Props
		$this->interest
			= $this->daysOverdue
			= $this->dailyInterest
			= $this->interestRate
			= $this->referenceRate
			= $this->grandTotal
			= NULL;
		$this->dueDate = $date;
		return $this;
	}
	
	/**
	 * Return the due date or calculate it based on values for terms and invoice date
	 * @return DateTime
	 */
	public function getDueDate() {
		if($this->dueDate instanceof DateTime) {
			return $this->dueDate;
		}
		if(!is_numeric($this->terms) || !$this->invoiceDate instanceof DateTime) {
			throw new Exception\RuntimeException("Both the invoice date and payment terms must be set in order to calculate a due date");
		}
		$date = clone($this->invoiceDate);
		$date->add(new DateInterval("P{$this->terms}D"));
		$this->setDueDate($date);
		return $date;
	}
	
	/**
	 * Set the invoice date
	 * @param string|DateTime $date
	 * @return self
	 */
	public function setInvoiceDate($date) {
		if(!$date instanceof DateTime) {
			$date = new DateTime($date);
		}
		// Reset Affected Props
		$this->interest
			= $this->daysOverdue
			= $this->dailyInterest
			= $this->interestRate
			= $this->referenceRate
			= $this->grandTotal
			= NULL;
			
		$this->invoiceDate = $date;
		return $this;
	}
	
	/**
	 * Return the invoice date or create it if we have both due date and terms available
	 * @return DateTime
	 * @throws Exception\ExceptionInterface if the invoice date is neither set, nor can it be deduced
	 */
	public function getInvoiceDate() {
		if($this->invoiceDate instanceof DateTime) {
			return $this->invoiceDate;
		}
		if(!is_numeric($this->terms) || !$this->dueDate instanceof DateTime) {
			throw new Exception\RuntimeException("The inoice date cannot be calculated without both the terms and the due date set");
		}
		$date = clone($this->dueDate);
		$date->sub(new DateInterval("P{$this->terms}D"));
		$this->setInvoiceDate($date);
		return $this->invoiceDate;
	}
	
	/**
	 * Set the terms in days
	 * @param int $days
	 * @return self
	 * @throws Exception\InvalidArgumentException if $days is not a number
	 */
	public function setTerms($days) {
		if(!is_numeric($days)) {
			$type = gettype($days);
			$type .= is_scalar($days) ? " {$days}" : "";
			throw new Exception\InvalidArgumentException("Terms should be an integer representing the number of days. Received {$type}");
		}
		
		$days = (int) $days;
		
		// Reset Affected props
		if($days !== $this->terms) {
			$this->interest
				= $this->daysOverdue
				= $this->interestRate
				= $this->dailyInterest
				= $this->grandTotal
				= NULL;
		}
			
		$this->terms = $days;
		return $this;
	}
	
	/**
	 * Return the terms as an integer in days
	 * @return int|NULL
	 */
	public function getTerms() {
		return $this->terms;
	}
	
	/**
	 * Return the statutory interest rate
	 * @return float
	 */
	public function getStatutoryInterestRate() {
		return $this->statutoryInterest;
	}
	
	/**
	 * Return the statutory interest rate
	 * @param float $rate
	 * @retun self
	 * @throws Exception\InvalidArgumentException if $rate is not a number
	 */
	public function setStatutoryInterestRate($rate) {
		if(!is_numeric($rate)) {
			$type = gettype($rate);
			$type .= is_scalar($rate) ? " {$rate}" : "";
			throw new Exception\InvalidArgumentException("Statutory interest rate should be a floating point number. Received {$type}");
		}
		
		// Reset Affected props
		$this->interest
			= $this->interestRate
			= $this->dailyInterest
			= $this->grandTotal
			= NULL;
			
		$this->statutoryInterest = (float) $rate;
		return $this;
	}
	
	/**
	 * Set the invoice amount
	 * @param float $amount
	 * @return self
	 * @throws Exception\InvalidArgumentException if $amount is not a number
	 */
	public function setInvoiceAmount($amount) {
		if(!is_numeric($amount)) {
			$type = gettype($amount);
			$type .= is_scalar($amount) ? " {$amount}" : "";
			throw new Exception\InvalidArgumentException("Invoice amount should be a floating point number. Received {$type}");
		}
		$this->invoiceAmount = (float) $amount;
		
		// Reset Affected props
		$this->fee
			= $this->interest
			= $this->dailyInterest
			= $this->grandTotal
			= NULL;
		
		return $this;
	}
	
	/**
	 * Return invoice amount
	 * @return float|NULL
	 */
	public function getInvoiceAmount() {
		return $this->invoiceAmount;
	}
	
	/**
	 * Return the debt recovery fee
	 * @return float
	 */
	public function getFee() {
		if(!$this->fee) {
			if($this->getDaysOverdue() === 0 || !$this->getInvoiceAmount()) {
				return 0.0;
			}
			foreach(array_reverse($this->fixedFees) as $fee) {
				if($this->getInvoiceAmount() >= $fee['minValue']) {
					$this->fee = $fee['amount'];
					break;
				}
			}
		}
		return $this->fee;
	}
	
	/**
	 * Return the reference rate used for calculation
	 * @return float
	 */
	public function getReferenceRate() {
		if(!$this->referenceRate) {
			$this->referenceRate = $this->getReferenceRateOnDate($this->getDueDate());
		}
		return $this->referenceRate;
	}
	
	/**
	 * Return the total interest chargeable
	 * @return float
	 */
	public function getInterest() {
		if(!$this->interest) {
			$result = $this->getDaysOverdue() * $this->getDailyInterest();
			$this->interest = round($result, 2, PHP_ROUND_HALF_UP);
		}
		return $this->interest;
	}
	
	/**
	 * Return the number of days overdue the invoice is
	 * @return int
	 * @throws Exception\ExceptionInterface if the value cannot be computed - i.e. we're missing property values
	 */
	public function getDaysOverdue() {
		/**
		 * The due date is required, or both the terms and the invoice date
		 */
		if(!$this->dueDate && (!$this->invoiceDate || !is_numeric($this->terms))) {
			throw new Exception\InvalidArgumentException("Either the due date must be set, or both the invoice date and payment terms");
		}
		if($this->dueDate) {
			$start = clone($this->dueDate);
		} else {
			$start = clone($this->invoiceDate);
			$start->add(new DateInterval("P{$this->terms}D"));
		}
		$diff = $start->diff(new DateTime);
		if($diff->invert === 1) {
			$this->daysOverdue = 0;
		} else {
			$this->daysOverdue = $diff->days;
		}
		return $this->daysOverdue;
	}
	
	/**
	 * Return the interest amount chargeable by the day
	 * @return float
	 * @throws Exception\InvalidArgumentException if invoice amount has not been set
	 */
	public function getDailyInterest() {
		if(!$this->dailyInterest) {
			if(empty($this->invoiceAmount)) {
				throw new Exception\RuntimeException('Daily interest cannot be calculated if an invoice amount has not been set');
			}
			$result = ($this->getInvoiceAmount() / 100) * ($this->getInterestRate() / 365);
			$this->dailyInterest = $result;
		}
		return $this->dailyInterest;
	}
	
	/**
	 * Return the sum of the statutory rate and the relevant reference rate
	 * @return float
	 */
	public function getInterestRate() {
		if(!$this->interestRate) {
			$this->interestRate = $this->getStatutoryInterestRate() + $this->getReferenceRate();
		}
		return $this->interestRate;
	}
	
	/**
	 * Return the total amount payable, i.e. the invoice + interest + debt recovery fee
	 * @return float
	 */
	public function getGrandTotal() {
		if(!$this->grandTotal) {
			$total = $this->getInvoiceAmount();
			$total += $this->getInterest();
			$total += $this->getFee();
			$this->grandTotal = $total;
		}
		return $this->grandTotal;
	}
	
	/**
	 * Return an array copy of the object
	 * @return array
	 */
	public function getArrayCopy() {
		$df = $this->getDateFormat();
		$invDate = $this->getInvoiceDate();
		if($invDate) {
			$invDate = $invDate->format($df);
		}
		$dueDate = $this->getDueDate();
		if($dueDate) {
			$dueDate = $dueDate->format($df);
		}
		return array(
			'invoiceDate' => $invDate,
			'daysOverdue' => $this->getDaysOverdue(),
			'dueDate' => $dueDate,
			'terms' => $this->getTerms(),
			'invoiceAmount' => $this->getInvoiceAmount(),
			'statutoryInterestRate' => $this->getStatutoryInterestRate(),
			'referenceRate' => $this->getReferenceRate(),
			'fee' => $this->getFee(),
			'interest' => $this->getInterest(),
			'dailyInterest' => $this->getDailyInterest(),
			'grandTotal' => $this->getGrandTotal(),
		);
	}
	
	public function getInputFilter() {
		$factory = new InputFilterFactory();
		$inputFilter = $factory->createInputFilter(array(
			'invoiceDate' => array(
				'name' => 'invoiceDate',
				'required' => true,
				'validators' => array(
					array(
						'name' => 'date',
						'options' => array(
							'format' => $this->getDateFormat(),
						),
					),
				),
			),
			'terms' => array(
				'name' => 'terms',
				'required' => true,
				'validators' => array(
					array(
						'name' => 'digits',
					),
					array(
						'name' => 'greaterthan',
						'options' => array(
							'min' => 0,
							'inclusive' => true,
						),
					),
				),
			),
			'invoiceAmount' => array(
				'name' => 'invoiceAmount',
				'required' => true,
				'validators' => array(
					array(
						'name' => 'float',
					),
					array(
						'name' => 'greaterthan',
						'options' => array(
							'min' => 1,
						),
					),
				),
			),
		));
		return $inputFilter;
	}
	
}