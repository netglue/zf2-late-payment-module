<?php
/**
 * A factory for setting up an instance of the BOE Rate Service
 */

namespace NetglueLatePayment\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FeeCalculatorFactory implements FactoryInterface {
	
	
	/**
	 * Create service
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return FeeCalculator|false
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		
		$service = $serviceLocator->get('NetglueLatePayment\Service\BOERateService');
		$calculator = new FeeCalculator;
		$calculator->setRateService($service);
		return $calculator;
	}
	
}
