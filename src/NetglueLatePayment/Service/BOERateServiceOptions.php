<?php

namespace NetglueLatePayment\Service;

use NetglueLatePayment\Exception;

use Zend\StdLib\AbstractOptions;

class BOERateServiceOptions extends AbstractOptions {
	
	protected $storageDirectory = '/tmp';
	
	protected $rateFileName = 'BankOfEnglandBaseRates.json';
	
	public function setStorageDirectory($dir) {
		if(!is_dir($dir)) {
			throw new Exception\InvalidArgumentException("Rate storage directory {$dir} is not a directory");
		}
		if(!is_writable($dir)) {
			throw new Exception\InvalidArgumentException("Rate storage directory {$dir} cannot be written to");
		}
		$this->storageDirectory = $dir;
		return $this;
	}
	
	public function getStorageDirectory() {
		return $this->storageDirectory;
	}
	
	public function setRateFileName($file) {
		if(!is_string($file)) {
			throw new Exception\InvalidArgumentException("Rate file name should be a string");
		}
		$this->rateFileName = $file;
		return $this;
	}
	
	public function getRateFileName() {
		return $this->rateFileName;
	}
	
}