<?php
/**
 * A client that retrieves the bank of england base rate from http://www.bankofengland.co.uk/
 */

// All available dates
// ?Travel=NIxRPxSUx&FromSeries=1&ToSeries=50&DAT=ALL&VFD=Y&CSVF=TT&C=13T&Filter=N&xml.x=1&xml.y=1
// Specific Range of dates
// ?Travel=NIxRPxSUx&FromSeries=1&ToSeries=50&DAT=RNG&FD=1&FM=Jan&FY=1998&TD=1&TM=Sep&TY=2013&VFD=N&xml.x=22&xml.y=26&CSVF=TT&C=13T&Filter=N

/**
 * Rates are delivered daily except bank holidays and weekends
 * First and last observations are always included at the same depth as rate data:
 * <Cube FIRST_OBS="1975-01-02" LAST_OBS="2013-09-04"/>
 */

namespace NetglueLatePayment\Service;

use NetglueLatePayment\Exception;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request as HttpRequest;
use DateTime;
use DateTimezone;

class BOERateClient {
	
	/**
	 * The end point of the BoE Website where we can get rate information
	 * @var string
	 */
	protected $feedUri = 'http://www.bankofengland.co.uk/boeapps/iadb/fromshowcolumns.asp';
	
	/**
	 * Zend Http Client
	 * @var HttpClient
	 */
	protected $client;
	
	/**
	 * Timezone for Rates
	 * @var DateTimezone
	 */
	protected $tz;
	
	/**
	 * Constructor
	 * @return void
	 */
	public function __construct() {
		$this->tz = new DateTimezone('Europe/London');
	}
	
	/**
	 * Return the timezone used for date calculation
	 * @return DateTimeZone
	 */
	public function getTimezone() {
		return $this->tz;
	}
	
	/**
	 * Return all available rates
	 * @return array
	 */
	public function findAllRates() {
		return $this->ratesToArray($this->getRateFile());
	}
	
	/**
	 * Return rate data betwen the two given dates
	 * @param string|DateTime $from
	 * @param string|DateTime $to
	 * @return array
	 */
	public function findRatesBetween($from, $to = NULL) {
		if(!$from instanceof DateTime) {
			$from = new DateTime($from, $this->tz);
		}
		if(!$to instanceof DateTime) {
			$to = new DateTime($to, $this->tz);
		}
		
		return $this->ratesToArray($this->getRateFile($from, $to));
	}
	
	/**
	 * Traverses the given XML string in the format expected from the BoE Website and returns an array
	 * @param string $xml
	 * @return array
	 */
	protected function ratesToArray($xml) {
		$reader = new \XMLReader;
		$reader->XML($xml);
		$rates = array();
		while($reader->read()) {
			if($reader->nodeType === \XmlReader::ELEMENT) {
				if($reader->name === 'Cube' && $reader->getAttribute('FIRST_OBS')) {
					$rates['info'] = array(
						'firstObservation' => $reader->getAttribute('FIRST_OBS'),
						'lastObservation' => $reader->getAttribute('LAST_OBS'),
					);
				}
				if($reader->name === 'Cube' && $reader->getAttribute('TIME')) {
					$entry = array();
					$entry['date'] = $reader->getAttribute('TIME');
					$date = new DateTime($entry['date'], $this->tz);
					$entry['dateTime'] = $date;
					$entry['rate'] = $reader->getAttribute('OBS_VALUE');
					$key = $date->format("Ymd");
					$rates[$key] = $entry;
				}
			}
		}
		return $rates;
	}
	
	/**
	 * Return XML from the remote optionally between two specific dates
	 * @param DateTime $from
	 * @param DateTime $to
	 * @return string
	 */
	protected function getRateFile(DateTime $from = NULL, DateTime $to = NULL) {
		$client = $this->getHttpClient();
		$client->reset();
		
		$params = $this->getUrlParams($from, $to);
		
		$client->setParameterGet($params);
		$client->setMethod(HttpRequest::METHOD_GET);
		$response = $client->send();
		
		// The remote returns text/html with status 200 on error
		$ctype = $response->getHeaders()->get('contenttype');
		$ctype = $ctype->toString();
		if(strpos(strtolower($ctype), 'xml') === false) {
			throw new Exception\RuntimeException("The remote BoE Website did not return XML");
		}
		return $response->getBody();
	}
	
	/**
	 * Return an array of parameters used in the GET request to the BOE site based on the given dates
	 * @param DateTime $from
	 * @param DateTime $to
	 * @return array
	 * @throws Exception\ExceptionInterface if the dates are screwy
	 */
	public function getUrlParams(DateTime $from = NULL, DateTime $to = NULL) {
		$params = array(
			'Travel' => 'NIxRPxSUx',
			'FromSeries' => '1',
			'ToSeries' => '50',
			'VFD' => 'N',
			'xml.x' => 1,
			'xml.y' => 1,
			'CSVF' => 'TT',
			'C' => '13T',
			'Filter' => 'N',
		);
		// No $to? Assume $from -> now()
		if(NULL !== $from && NULL === $to) {
			$to = new DateTime;
		}
		// No $from? Return all available data
		if(NULL === $from) {
			$params = array_merge($params, array(
				'DAT' => 'ALL',
			));
		} else {
			// Otherwise populate date parameters accordingly after checking date validity
			if($from->getTimestamp() > $to->getTimestamp()) {
				throw new Exception\InvalidArgumentException("The from date cannot be after the to date");
			}
			$params = array_merge($params, array(
				'DAT' => 'RNG',
				'FD' => $from->format("j"),
				'FM' => $from->format("M"),
				'FY' => $from->format("Y"),
				'TD' => $to->format("j"),
				'TM' => $to->format("M"),
				'TY' => $to->format("Y"),
			));
		}
		return $params;
	}
	
	/**
	 * Return Http Client
	 * @return HttpClient
	 */
	public function getHttpClient() {
		if(!$this->client) {
			$this->client = new HttpClient($this->feedUri);
		}
		return $this->client;
	}
	
}