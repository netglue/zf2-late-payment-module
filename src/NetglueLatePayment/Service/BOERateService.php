<?php

namespace NetglueLatePayment\Service;

use NetglueLatePayment\Exception;
use DateTime;
use DateTimezone;

class BOERateService {

	/**
	 * BoE Rate Data
	 * @var array|NULL
	 */
	protected $rates;

	/**
	 * Rate Client, loads XML from remote
	 * @var BOERateClient|NULL
	 */
	protected $rateClient;

	/**
	 * Timezone for Rates
	 * @var DateTimezone
	 */
	protected $tz;

	/**
	 * Service Options
	 * @var BOERateServiceOptions
	 */
	protected $options;

	/**
	 * Constructor
	 * @return void
	 */
	public function __construct() {
		$this->tz = new DateTimezone('Europe/London');
	}

	/**
	 * Return all currently stored rates
	 * @return array
	 */
	public function getRates() {
		if(NULL === $this->rates) {
			if(!$this->loadData()) {
				// We need to import rates
				$client = $this->getRateClient();
				$data = $client->findAllRates();
				$this->transformRateData($data);
			}
		}
		return $this->rates;
	}

	/**
	 * Return the BoE Base rate as of now
	 * @return float
	 */
	public function getCurrentRate() {
		$rates = $this->getRates();
		unset($rates['info']);
		$rate = end($rates);
		return (float) $rate['rate']; // Just incase serialized to int
	}

	/**
	 * Return the date that the rate last changed
	 * @return DateTime
	 */
	public function getLastChangeDate() {
		$rates = $this->getRates();
		unset($rates['info']);
		$rate = end($rates);
		return new DateTime($rate['date'], $this->tz);
	}

	/**
	 * Return the BoE base rate on a specific date
	 * @param string|DateTime $date
	 * @return float|false
	 */
	public function getRateOnDate($date) {
		if(!$date instanceof DateTime) {
			$date = new DateTime($date, $this->tz);
		}
		$lastChange = $this->getLastChangeDate();
		$diff = $date->diff($lastChange);
		if($diff->invert === 1 || $diff->days === 0) {
			return $this->getCurrentRate();
		}
		$key = $date->format("Ymd");
		$last = NULL;
		$all = $this->getRates();
		unset($all['info']);
		$dates = array_keys($all);
		$min = min($dates);

		if($key < $min) {
			// The date precedes the earliest rate on record
			return false;
		}
		foreach($all as $dk => $data) {
			if($dk > $key && $last !== NULL) {
				return (float) $last['rate'];
			}
			$last = $data;
		}
		// ?
		return false;
	}

	/**
	 * Return the first observation date
	 * @return DateTime|false
	 */
	public function getFirstObservationDate() {
		$rates = $this->getRates();
		if(isset($rates['info']['firstObservation'])) {
			return new DateTime($rates['info']['firstObservation'], $this->tz);
		}
		return false;
	}

	/**
	 * Return the last observation date
	 * @return DateTime|false
	 */
	public function getLastObservationDate() {
		$rates = $this->getRates();
		if(isset($rates['info']['lastObservation'])) {
			return new DateTime($rates['info']['lastObservation'], $this->tz);
		}
		return false;
	}

	/**
	 * Update the rate data stored if required
	 * @return void
	 */
	protected function updateRates() {
		$allRates = $this->getRates();
		$last = new DateTime($allRates['info']['lastObservation'], $this->tz);
		$now = new DateTime(NULL, $this->tz);
		// If we already checked for rate changes today, don't bother again until tomorrow.
		if(isset($allRates['info']['lastCheck'])) {
			$lastCheck = new DateTime($allRates['info']['lastCheck'], $this->tz);
			$diff = $now->diff($lastCheck);
			if($diff->invert === 1 && $diff->d === 0) {
				return;
			}
		}

		// Get rate data between last observation and now and add it to the rates
		$diff = $now->diff($last);
		if($diff->invert === 1 && $diff->d >=1) {
			$client = $this->getRateClient();
			$data = $client->findRatesBetween($last, $now);
			$this->rates['info']['lastCheck'] = $now->format("Y-m-d");
			$this->transformRateData($data);
		}
	}

	/**
	 * Return the simple client that's responsible for getting data from the BoE
	 * @return BOERateClient
	 */
	public function getRateClient() {
		if(!$this->rateClient) {
			$this->rateClient = new BOERateClient;
		}
		return $this->rateClient;
	}

	/**
	 * Sorts and filters rate data so that we only have rates for each of the days a change occurred
	 * @param array $data
	 * @return void
	 */
	protected function transformRateData($data) {
		if(!is_array($this->rates)) {
			$this->rates = array();
		}
		if(isset($data['info'])) {
			if(!isset($this->rates['info'])) {
				$this->rates['info'] = array();
			}
			$this->rates['info'] = array_merge($this->rates['info'], $data['info']);
			unset($data['info']);
		}
		foreach($data as $key => $rate) {
			$o = array();
			$o['date'] = $rate['date'];
			$o['timezone'] = $rate['dateTime']->getTimezone()->getName();
			$o['rate'] = (float) $rate['rate'];
			$this->rates[$key] = $o;
		}
		ksort($this->rates, SORT_NUMERIC);
		/**
		 * Filter out all data except changes to the rates
		 */
		$cur = NULL;
		foreach($this->rates as $key => $day) {
			if($key === 'info') {
				continue;
			}
			if(NULL === $cur) {
				$cur = $day['rate'];
				continue;
			}
			if($day['rate'] === $cur) {
				unset($this->rates[$key]);
			} else {
				$cur = $day['rate'];
			}
		}
		$this->saveData();
	}

	/**
	 * Set options
	 *
	 * @param  array|Traversable|BOERateServiceOptions $options
	 * @return BOERateService
	 * @see getOptions()
	 */
	public function setOptions($options) {
		if($this->options !== $options) {
			if (!$options instanceof BOERateServiceOptions) {
				$options = new BOERateServiceOptions($options);
			}
			$this->options = $options;
		}
		return $this;
	}

	/**
	 * Return configured options object
	 * @return BOERateServiceOptions|NULL
	 */
	public function getOptions() {
		if (!$this->options) {
			$this->setOptions(new BOERateServiceOptions());
		}
		return $this->options;
	}

	/**
	 * Load the rate file on disk into the rates property
	 * @return void
	 */
	protected function loadData() {
		$opt = $this->getOptions();
		$file = rtrim($opt->getStorageDirectory(), '/').'/'.$opt->getRateFileName();
		if(file_exists($file)) {
			$this->rates = json_decode(file_get_contents($file), true);
			$this->updateRates();
			return true;
		}
		return false;
	}

	/**
	 * Persist rate data to configured JSON file
	 * @return bool
	 */
	protected function saveData() {
		$opt = $this->getOptions();
		$file = rtrim($opt->getStorageDirectory(), '/').'/'.$opt->getRateFileName();
		$bytes = file_put_contents($file, json_encode($this->rates));
		if(false === $bytes) {
			throw new Exception\RuntimeException("Failed to write BOE rate data to file");
		}
		return true;
	}

}
