<?php

namespace NetglueLatePayment\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface {
	
}