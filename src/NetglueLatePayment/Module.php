<?php

namespace NetglueLatePayment;

/**
 * Autoloader
 */
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Loader\AutoloaderFactory;
use Zend\Loader\StandardAutoloader;

/**
 * Service Provider
 */
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * Config Provider
 */
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Controller Provider
 */
use Zend\ModuleManager\Feature\ControllerProviderInterface;

/**
 * Module...
 * @codeCoverageIgnore
 */
class Module implements
	AutoloaderProviderInterface,
	ServiceProviderInterface,
	ConfigProviderInterface,
	ControllerProviderInterface {


	/**
	 * Return autoloader configuration
	 * @link http://framework.zend.com/manual/2.0/en/user-guide/modules.html
	 * @return array
	 */
	public function getAutoloaderConfig() {
        return array(
			AutoloaderFactory::STANDARD_AUTOLOADER => array(
				StandardAutoloader::LOAD_NS => array(
					__NAMESPACE__ => __DIR__,
				),
			),
		);
	}

	/**
	 * Return Controller Config
	 * @return array
	 */
	public function getControllerConfig() {
		return array(
			'invokables' => array(
				'NetglueLatePayment\Controller\RateController' => 'NetglueLatePayment\Controller\RateController',
				'NetglueLatePayment\Controller\FeeController' => 'NetglueLatePayment\Controller\FeeController',
			),
		);
	}

	/**
	 * Include/Return module configuration
	 * @return array
	 * @implements ConfigProviderInterface
	 */
	public function getConfig() {
		return include __DIR__ . '/../../config/module.config.php';
	}

	/**
	 * Return Service Config
	 * @return array
	 * @implements ServiceProviderInterface
	 */
	public function getServiceConfig() {
	    return array(
            'factories' => array(
                'NetglueLatePayment\Service\BOERateService' => 'NetglueLatePayment\Service\BOERateServiceFactory',
                'NetglueLatePayment\Service\FeeCalculator' => 'NetglueLatePayment\Service\FeeCalculatorFactory',
            ),

            'aliases' => array(
                'boeRates' => 'NetglueLatePayment\Service\BOERateService',
                'latePaymentCalculator' => 'NetglueLatePayment\Service\FeeCalculator',
            ),
        );
	}
}
