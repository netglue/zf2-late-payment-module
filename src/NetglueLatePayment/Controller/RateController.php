<?php

namespace NetglueLatePayment\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use NetglueLatePayment\Service\BOERateService;

class RateController extends AbstractActionController {
	
	/**
	 * Rate Service
	 * @var BOERateService|NULL
	 */
	protected $rateService;
	
	/**
	 * Default rate service html view displays historical and current rates
	 * @return ViewModel
	 */
	public function indexAction() {
		$service = $this->getRateService();
		$view = new ViewModel(array(
			'rates' => $service->getRates(),
			'currentRate' => $service->getCurrentRate(),
			'lastChangeDate' => $service->getLastChangeDate(),
		));
		return $view;
	}
	
	/**
	 * Action returns the BoE Base rate on a given day or an error object
	 * @return JsonModel
	 */
	public function jsonAction() {
		$params = $this->params()->fromRoute();
		if(!isset($params['year']) || !preg_match('/^[0-9]{4}$/', $params['year'])) {
			return $this->returnError('Invalid year specified', 400);
		}
		if(!isset($params['month']) || !preg_match('/^[0-9]{1,2}$/', $params['month']) || $params['month'] < 1 || $params['month'] > 12) {
			return $this->returnError('Invalid month specified', 400);
		}
		if(!isset($params['day']) || !preg_match('/^[0-9]{1,2}$/', $params['day']) || $params['day'] < 1 || $params['day'] > 31) {
			return $this->returnError('Invalid day specified', 400);
		}
		$service = $this->getRateService();
		$date = sprintf('%d-%d-%d', $params['year'], $params['month'], $params['day']);
		try {
			$rate = $service->getRateOnDate($date);
		} catch(\NetglueLatePayment\Exception\ExceptionInterface $e) {
			return $this->returnError('Internal Error', 500);
		}
		if(false === $rate) {
			return $this->returnError('No rate found for the specified date. Records start from approx 1975', 400);
		}
		
		return new JsonModel(array(
			'rate' => $rate,
			'error' => false,
		));
	}
	
	/**
	 * Return request Errors in a standardised way
	 * @param string $msg Error Message
	 * @param int $code HTTP Status Code
	 * @return JsonModel
	 */
	protected function returnError($msg, $code) {
		$this->getResponse()->setStatusCode($code);
		return new JsonModel(array(
				'error' => true,
				'message' => $msg,
				'code' => $code,
			));
	}
	
	
	public function setRateService(BOERateService $service) {
		$this->rateService = $service;
		return $this;
	}
	
	public function getRateService() {
		if(NULL === $this->rateService) {
			$sl = $this->getServiceLocator();
			$service = $sl->get('NetglueLatePayment\Service\BOERateService');
			$this->setRateService($service);
		}
		return $this->rateService;
	}
	
	
}