<?php

namespace NetglueLatePayment\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use NetglueLatePayment\Service\FeeCalculator;

use Zend\Stdlib\Hydrator\ClassMethods;

class FeeController extends AbstractActionController {

	/**
	 * The Late Payment Calculator
	 * @var FeeCalculator|NULL
	 */
	protected $calculator;

	/**
	 * Render default view script
	 * @return ViewModel
	 */
	public function indexAction() {
		return new ViewModel();
	}

	/**
	 * Given posted params, use the calculator to return a JSON object with the results
	 * @return JsonModel
	 */
	public function jsonAction() {
		$request = $this->getRequest();
		if(!$request->isPost()) {
			return $this->jsonError("Expected a POST request", 400);
		}

		$post = $request->getPost();
		$calc = $this->getFeeCalculator();
		$filter = $calc->getInputFilter();
		$filter->setData($post);
		if(!$filter->isValid()) {
			return $this->jsonError(
				'Invalid values provided to the calculator',
				400,
				$filter->getMessages()
			);
		}
		try {
			$hydrator = new ClassMethods;
			$hydrator->hydrate($filter->getValues(), $calc);
			return new JsonModel($calc->getArrayCopy());
		} catch(\Exception $e) {
			return $this->jsonError(
				$e->getMessage(),
				500
			);
		}
	}

	/**
	 * Return request Errors in a standardised way
	 * @param string $msg Error Message
	 * @param int $code HTTP Status Code
	 * @return JsonModel
	 */
	protected function jsonError($msg, $code, $errors = array()) {
		$this->getResponse()->setStatusCode($code);
		return new JsonModel(array(
            'error' => true,
            'message' => $msg,
            'code' => $code,
            'errors' => $errors,
        ));
	}

	/**
	 * Set the Feee Calculator instance to use
	 * @param FeeCalculator $calculator
	 * @return self
	 */
	public function setFeeCalculator(FeeCalculator $calculator) {
		$this->calculator = $calculator;
		return $this;
	}

	/**
	 * Return Fee Calculator Instance
	 *
	 * The get|setFeeCalculator methods are only really here if we were to decide
	 * to use a factory to setup this controller. For now, we're just getting
	 * the calculator from $this->getServiceLocator()
	 *
	 * @return FeeCalculator
	 * @throws \Zend\ServiceManager\Exception\ExceptionInterface
	 */
	public function getFeeCalculator() {
		if(NULL === $this->calculator) {
			$sl = $this->getServiceLocator();
			$service = $sl->get('NetglueLatePayment\Service\FeeCalculator');
			$this->setFeeCalculator($service);
		}
		return $this->calculator;
	}

}
