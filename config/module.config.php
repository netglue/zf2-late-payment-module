<?php

return array(

	/**
	 * Settings for Bank of England Base Rate Service
	 *
	 * Set storage directory to a writable directory...
	 * You can also change the default file name containing the JSON data
	 * @see NetglueLatePayment\Service\BOERateServiceOptions
	 */
	'boe_rates' => array(
		'storageDirectory' => '/tmp',
	),

	/**
	 * Routes
	 * To override in your app, you can specify:
	 * ['router']['routes']['boe_rates']['options']['route'] = '/some-place-else'
	 * ['router']['routes']['late_payment']['options']['route'] = '/some-place-else'
	 */
	'router' => array(
		'routes' => array(
			'boe_rates' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/bank-of-england-base-rate',
					'defaults' => array(
						'controller' => 'NetglueLatePayment\Controller\RateController',
						'action' => 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'json' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/json[/:year[/:month[/:day]]]',
							'defaults' => array(
								'controller' => 'NetglueLatePayment\Controller\RateController',
								'action' => 'json',
							),
						),
					),
				),
			),
			'late_payment' => array(
				'type' => 'Literal',
				'options' => array(
					'route' => '/late-payment-interest-calculator',
					'defaults' => array(
						'controller' => 'NetglueLatePayment\Controller\FeeController',
						'action' => 'index',
					),
				),
				'may_terminate' => true,
				'child_routes' => array(
					'json' => array(
						'type' => 'Segment',
						'options' => array(
							'route' => '/json',
							'defaults' => array(
								'controller' => 'NetglueLatePayment\Controller\FeeController',
								'action' => 'json',
							),
						),
					),
				),
			),
		),
	),

	/**
	 * View Scripts
	 *
	 * Overriding the view scripts: Use the config keys:
	 * ['view_manager']['template_map']['netglue-late-payment/rate/index'] = '/some/view/script'
	 * ['view_manager']['template_map']['netglue-late-payment/fee/index'] = '/some/view/script'
	 */
	'view_manager' => array(
		'template_map' => array(
			'netglue-late-payment/rate/index' => __DIR__ . '/../view/netglue-late-payment/rate/index.phtml',
			'netglue-late-payment/fee/index' => __DIR__ . '/../view/netglue-late-payment/fee/index.phtml',
		),
		'strategies' => array(
            'ViewJsonStrategy'
        )
	),
);
