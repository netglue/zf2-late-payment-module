<?php
namespace NetglueLatePaymentTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * @coversDefaultClass NetglueLatePayment\Controller\RateController
 */
class RateControllerTest extends AbstractHttpControllerTestCase {

	public function setUp()
    {
        $this->setTraceError(true);
        $this->setApplicationConfig(include __DIR__ . '/../../TestConfig.php.dist');
        parent::setUp();
    }

    public function testIndexActionIsAccessible()
	{
	    $this->dispatch('/bank-of-england-base-rate');
	    $this->assertResponseStatusCode(200);
	}

	public function testJsonActionRequiresYear()
	{
	    $this->dispatch('/bank-of-england-base-rate/json');
	    $this->assertResponseStatusCode(400);
	    $this->assertHasResponseHeader('Content-Type');
        $this->assertResponseHeaderContains('Content-Type', 'application/json; charset=utf-8');
        $body = json_decode($this->getResponse()->getContent());
        $this->assertInstanceOf('stdClass', $body);
        $this->assertEquals('Invalid year specified', $body->message);
	}

    public function testJsonActionRequiresMonth()
	{
	    $this->dispatch('/bank-of-england-base-rate/json/2015');
	    $this->assertResponseStatusCode(400);
	    $this->assertHasResponseHeader('Content-Type');
        $this->assertResponseHeaderContains('Content-Type', 'application/json; charset=utf-8');
        $body = json_decode($this->getResponse()->getContent());
        $this->assertInstanceOf('stdClass', $body);
        $this->assertEquals('Invalid month specified', $body->message);
	}

	public function testJsonActionRequiresDay()
	{
	    $this->dispatch('/bank-of-england-base-rate/json/2015/01');
	    $this->assertResponseStatusCode(400);
	    $this->assertHasResponseHeader('Content-Type');
        $this->assertResponseHeaderContains('Content-Type', 'application/json; charset=utf-8');
        $body = json_decode($this->getResponse()->getContent());
        $this->assertInstanceOf('stdClass', $body);
        $this->assertEquals('Invalid day specified', $body->message);
	}

	public function testJsonActionReturnsErrorForOldDates()
	{
	    $this->dispatch('/bank-of-england-base-rate/json/1920/01/01');
	    $this->assertResponseStatusCode(400);
	    $this->assertHasResponseHeader('Content-Type');
        $this->assertResponseHeaderContains('Content-Type', 'application/json; charset=utf-8');
        $body = json_decode($this->getResponse()->getContent());
        $this->assertInstanceOf('stdClass', $body);
        $this->assertStringStartsWith('No rate found for the specified date', $body->message);
	}

	public function testValidJsonResponse()
	{
	    $this->dispatch('/bank-of-england-base-rate/json/2014/01/01');
	    $this->assertResponseStatusCode(200);
	    $this->assertHasResponseHeader('Content-Type');
        $this->assertResponseHeaderContains('Content-Type', 'application/json; charset=utf-8');
        $body = json_decode($this->getResponse()->getContent());
        $this->assertInstanceOf('stdClass', $body);
        $this->assertEquals(0.5, $body->rate);
	}

}
