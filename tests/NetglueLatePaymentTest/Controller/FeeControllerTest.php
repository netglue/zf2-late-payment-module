<?php
namespace NetglueLatePaymentTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class FeeControllerTest extends AbstractHttpControllerTestCase {


	public function setUp()
    {
        $this->setTraceError(true);
        $this->setApplicationConfig(include __DIR__ . '/../../TestConfig.php.dist');
        parent::setUp();
    }

    public function testCalculatorIsAccessible()
    {
        $sl = $this->getApplicationServiceLocator();
        $manager = $sl->get('ControllerManager');
        $controller = $manager->get('NetglueLatePayment\Controller\FeeController');
        $this->assertInstanceOf('NetglueLatePayment\Service\FeeCalculator', $controller->getFeeCalculator());
    }

	public function testJsonActionReturnsErrorForNoPost() {
	    $this->dispatch('/late-payment-interest-calculator/json');
        $this->assertResponseStatusCode(400);
        $this->assertHasResponseHeader('Content-Type');
        $this->assertResponseHeaderContains('Content-Type', 'application/json; charset=utf-8');
	}

	public function testJsonActionReturnsJsonForValidPost()
	{
	    $data = array(
	        'invoiceDate' => '2015-01-01',
	        'terms' => 30,
	        'invoiceAmount' => 100,
	    );

	    $this->dispatch('/late-payment-interest-calculator/json', 'POST', $data);
	    $this->assertResponseStatusCode(200);
	    $this->assertHasResponseHeader('Content-Type');
        $this->assertResponseHeaderContains('Content-Type', 'application/json; charset=utf-8');
        $body = json_decode($this->getResponse()->getContent());
        $this->assertInstanceOf('stdClass', $body);
        $attributes = array(
            'invoiceDate',
            'daysOverdue',
            'dueDate',
            'terms',
            'invoiceAmount',
            'statutoryInterestRate',
            'referenceRate',
            'fee',
            'interest',
            'dailyInterest',
            'grandTotal',
        );
        foreach($attributes as $attr) {
            $this->assertObjectHasAttribute($attr, $body);
        }
	}

	public function testJsonActionReturnsErrorForInvalidPost()
	{
	    $this->dispatch('/late-payment-interest-calculator/json', 'POST', array());
	    $this->assertResponseStatusCode(400);
        $this->assertHasResponseHeader('Content-Type');
        $this->assertResponseHeaderContains('Content-Type', 'application/json; charset=utf-8');
        $body = json_decode($this->getResponse()->getContent());
        $this->assertInstanceOf('stdClass', $body);
        $this->assertEquals('Invalid values provided to the calculator', $body->message);
	}

	public function testIndexAction()
	{
	    $this->dispatch('/late-payment-interest-calculator');
	    $this->assertResponseStatusCode(200);
	}


}
