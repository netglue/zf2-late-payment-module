<?php

namespace NetglueLatePaymentTest\Service;
use PHPUnit_Framework_TestCase;

use NetglueLatePayment\Service\BOERateService as RateService;
use NetglueLatePayment\Service\BOERateClient as RateClient;
use NetglueLatePayment\Service\BOERateServiceOptions as ServiceOptions;
use NetglueLatePayment\Service\FeeCalculator as Calc;


use DateTime;
use DateTimeZone;

/**
 * @coversDefaultClass NetglueLatePayment\Service\FeeCalculator
 */
class FeeCalculatorTest extends PHPUnit_Framework_TestCase {
	
	protected $rateService;
	
	/**
	 * Return a configured Rate Service for use with calculator
	 * Uses cached rate data in ../data/
	 * @return RateService
	 */
	public function getRateService() {
		if(!$this->rateService) {
			$this->rateService = new RateService;
			$this->rateService->setOptions(array(
				'storageDirectory' => __DIR__.'/../data',
			));
		}
		return $this->rateService;
	}
	
	/**
	 * @covers ::setRateService
	 * @covers ::getRateService
	 */
	public function testSetGetRateService() {
		$calc = new Calc;
		$rates = $this->getRateService();
		// Initially NULL
		$this->assertNull($calc->getRateService());
		// Fluid
		$this->assertSame($calc, $calc->setRateService($rates));
		// Same instance
		$this->assertSame($rates, $calc->getRateService());
	}
	
	/**
	 * Return a ready to use calculator with dependencies set
	 * @return Calc
	 */
	public function getCalc() {
		$calc = new Calc;
		$rates = $this->getRateService();
		$calc->setRateService($rates);
		return $calc;
	}
	
	/**
	 * @depends testSetGetRateService
	 * @covers ::getReferenceRateOnDate
	 */
	public function testGetReferenceRateOnDate() {
		// on 1979-11-15 the rate was 17%
		// so ref rate for anything up to 1980-06-30 should be 17
		$calc = $this->getCalc();
		$date = new DateTime('1980-01-01');
		$this->assertSame(17.0, $calc->getReferenceRateOnDate($date));
		// on 1980-07-03 the rate was 16% so the ref rate should remain at 17% for the rest of the year
		$date = new DateTime('1980-08-01');
		$this->assertSame(17.0, $calc->getReferenceRateOnDate($date));
		// Rate changed to 14% on 1980-11-25 so anything in the first half 1981 will be 14%
		$date = new DateTime('1981-01-01');
		$this->assertSame(14.0, $calc->getReferenceRateOnDate($date));
		// Rate changed to 12% on 1981-03-11 so while we should still be at 14% in June, July will be 12%
		$date = new DateTime('1981-06-01');
		$this->assertSame(14.0, $calc->getReferenceRateOnDate($date));
		$date = new DateTime('1981-07-01');
		$this->assertSame(12.0, $calc->getReferenceRateOnDate($date));
	}
	
	/**
	 * @depends testSetGetRateService
	 * @covers ::getReferenceRateOnDate
	 */
	public function testGetReferenceRateOnDateAcceptsStrings() {
		$calc = $this->getCalc();
		$date = '1980-01-01';
		$this->assertSame(17.0, $calc->getReferenceRateOnDate($date));
		$date = '1981-07-01';
		$this->assertSame(12.0, $calc->getReferenceRateOnDate($date));
	}
	
	/**
	 * @depends testSetGetRateService
	 * @covers ::getReferenceRateOnDate
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testGetReferenceRateThrowsExceptionForNotFoundRates() {
		$calc = $this->getCalc();
		$date = '1960-01-01';
		$calc->getReferenceRateOnDate($date);
	}
	
	/**
	 * @covers ::setDueDate
	 * @covers ::getDueDate
	 */
	public function testSetGetDueDate() {
		$calc = $this->getCalc();
		// Fluid
		$this->assertSame($calc, $calc->setDueDate(NULL));
		$this->assertInstanceOf('DateTime', $calc->getDueDate());
		// Given null or '' now() should be the date
		$due = $calc->getDueDate();
		$this->assertSame(date("Ymd"), $due->format("Ymd"));
		// Accepts reasonable date strings
		$calc->setDueDate('1980-01-01');
		$due = $calc->getDueDate();
		$this->assertSame('19800101', $due->format("Ymd"));
		
		// Accepts Date Objects
		$due = new DateTime('2012-06-01');
		$calc->setDueDate($due);
		$this->assertSame($due, $calc->getDueDate());
	}
	
	/**
	 * @covers ::getDueDate
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testGetDueDateThrowsExceptionWithoutInvoiceDateSet() {
		$calc = $this->getCalc();
		$calc->getDueDate();
	}
	
	/**
	 * @covers ::setInvoiceDate
	 * @covers ::getInvoiceDate
	 */
	public function testSetGetInvoiceDate() {
		$calc = $this->getCalc();
		// Fluid
		$this->assertSame($calc, $calc->setInvoiceDate(NULL));
		$this->assertInstanceOf('DateTime', $calc->getInvoiceDate());
		// Given null or '' now() should be the date
		$date = $calc->getInvoiceDate();
		$this->assertSame(date("Ymd"), $date->format("Ymd"));
		// Accepts reasonable date strings
		$calc->setInvoiceDate('1980-01-01');
		$date = $calc->getInvoiceDate();
		$this->assertSame('19800101', $date->format("Ymd"));
		
		// Accepts Date Objects
		$date = new DateTime('2012-06-01');
		$calc->setInvoiceDate($date);
		$this->assertSame($date, $calc->getInvoiceDate());
	}
	
	/**
	 * @covers ::getInvoiceDate
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testGetInvoiceDateThrowsExceptionWithoutDueDateSet() {
		$calc = $this->getCalc();
		$calc->getInvoiceDate();
	}
	
	/**
	 * @covers ::getTerms
	 * @covers ::setTerms
	 */
	public function testSetGetTermsBasic() {
		$calc = $this->getCalc();
		// Initially set to an integer
		$this->assertInternalType('int', $calc->getTerms());
		// fluid
		$this->assertSame($calc, $calc->setTerms(60));
		$this->assertSame(60, $calc->getTerms());
		// Accepts a zero
		$this->assertSame($calc, $calc->setTerms(0));
		$this->assertSame(0, $calc->getTerms());
	}
	
	/**
	 * @covers ::setTerms
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testSetTermsThrowsExceptionForNonNumbers() {
		$calc = $this->getCalc();
		$calc->setTerms(30);
		$invalid = array(
			array(),
			'foo',
			new \stdclass,
		);
		foreach($invalid as $param) {
			try {
				$calc->setTerms($param);
			} catch(NetglueLatePayment\Exception\ExceptionInterface $e) {
				$this->assertInstanceOf('NetglueLatePayment\Exception\ExceptionInterface', $e);
				$this->assertSame(30, $calc->getTerms());
			}
		}
	}
	
	/**
	 * @covers ::getDueDate
	 * @depends testSetGetInvoiceDate
	 * @depends testSetGetTermsBasic
	 */
	public function testGetDueDateCalculation() {
		$date = new DateTime('2013-01-01');
		$calc = $this->getCalc();
		$calc->setInvoiceDate($date);
		$calc->setTerms(30);
		$due = $calc->getDueDate();
		$this->assertInstanceOf('DateTime', $due);
		$this->assertSame('20130131', $due->format('Ymd'));
		
		$calc = $this->getCalc();
		$calc->setInvoiceDate($date);
		$calc->setTerms(0);
		$due = $calc->getDueDate();
		$this->assertInstanceOf('DateTime', $due);
		$this->assertSame('20130101', $due->format('Ymd'));
		
		$this->assertTrue( $due !== $date );
	}
	
	/**
	 * @covers ::getInvoiceDate
	 * @depends testSetGetDueDate
	 * @depends testSetGetTermsBasic
	 */
	public function testGetInvoiceDateCalculation() {
		$date = new DateTime('2013-01-31');
		$calc = $this->getCalc();
		$calc->setDueDate($date);
		$calc->setTerms(30);
		$inv = $calc->getInvoiceDate();
		$this->assertInstanceOf('DateTime', $inv);
		$this->assertSame('20130101', $inv->format('Ymd'));
		
		$calc = $this->getCalc();
		$calc->setDueDate($date);
		$calc->setTerms(0);
		$inv = $calc->getInvoiceDate();
		$this->assertInstanceOf('DateTime', $inv);
		$this->assertSame('20130131', $inv->format('Ymd'));
		
		$this->assertTrue( $inv !== $date );
	}
	
	/**
	 * @covers ::getStatutoryInterestRate
	 * @covers ::setStatutoryInterestRate
	 */
	public function testGetStatutoryInterestRateBasic() {
		$calc = $this->getCalc();
		// Initially set
		$this->assertInternalType('float', $calc->getStatutoryInterestRate());
		// Fluid setter
		$this->assertSame($calc, $calc->setStatutoryInterestRate(1));
		$this->assertSame(1.0, $calc->getStatutoryInterestRate());
	}
	
	/**
	 * @covers ::setStatutoryInterestRate
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testSetStatutoryInterestRateThrowsExceptionForNonNumbers() {
		$calc = $this->getCalc();
		$calc->setStatutoryInterestRate(1);
		$invalid = array(
			array(),
			'foo',
			new \stdclass,
		);
		foreach($invalid as $param) {
			try {
				$calc->setStatutoryInterestRate($param);
			} catch(NetglueLatePayment\Exception\ExceptionInterface $e) {
				$this->assertInstanceOf('NetglueLatePayment\Exception\ExceptionInterface', $e);
				$this->assertSame(1.0, $calc->getStatutoryInterestRate());
			}
		}
	}
	
	/**
	 * @covers ::getInvoiceAmount
	 * @covers ::setInvoiceAmount
	 */
	public function testGetInvoiceAmountBasic() {
		$calc = $this->getCalc();
		// Initially Null
		$this->assertNull($calc->getInvoiceAmount());
		// Fluid setter
		$this->assertSame($calc, $calc->setInvoiceAmount(1));
		$this->assertSame(1.0, $calc->getInvoiceAmount());
	}
	
	/**
	 * @covers ::setInvoiceAmount
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testSetInvoiceAmountThrowsExceptionForNonNumbers() {
		$calc = $this->getCalc();
		$calc->setInvoiceAmount(1);
		$invalid = array(
			array(),
			'foo',
			new \stdclass,
		);
		foreach($invalid as $param) {
			try {
				$calc->setInvoiceAmount($param);
			} catch(NetglueLatePayment\Exception\ExceptionInterface $e) {
				$this->assertInstanceOf('NetglueLatePayment\Exception\ExceptionInterface', $e);
				$this->assertSame(1.0, $calc->getInvoiceAmount());
			}
		}
	}
	
	/**
	 * @covers ::getDaysOverdue
	 */
	public function testGetDaysOverdueBasic() {
		$calc = $this->getCalc();
		$calc->setInvoiceDate('-1 day')->setTerms(0);
		$days = $calc->getDaysOverdue();
		$this->assertInternalType('int', $days);
		$this->assertSame(1, $days);
		
		$calc = $this->getCalc();
		$calc->setDueDate('-1 day');
		$days = $calc->getDaysOverdue();
		$this->assertInternalType('int', $days);
		$this->assertSame(1, $days);
	}
	
	/**
	 * @covers ::getDaysOverdue
	 */
	public function testGetDaysOverdueReturnsZeroForNonOverdue() {
		$calc = $this->getCalc();
		$calc->setInvoiceDate('now')->setTerms(30);
		$days = $calc->getDaysOverdue();
		$this->assertInternalType('int', $days);
		$this->assertSame(0, $days);
	}
	
	/**
	 * @covers ::getDaysOverdue
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testGetDaysOverdueThrowsExceptionIfInvoiceDateUnset() {
		$calc = $this->getCalc();
		$calc->getDaysOverdue();
	}
	
	/**
	 * @covers ::getReferenceRate
	 * @depends testGetReferenceRateOnDate
	 */
	public function testGetReferenceRate() {
		$calc = $this->getCalc();
		$date = new DateTime('1980-01-01');
		$calc->setDueDate($date);
		$this->assertSame(17.0, $calc->getReferenceRate());
	}
	
	/**
	 * @covers ::getReferenceRate
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testGetReferenceRateCausesExceptionWithoutDatesSet() {
		$calc = $this->getCalc();
		$calc->getReferenceRate();
	}
	
	/**
	 * @covers ::getInterestRate
	 * @depends testGetReferenceRate
	 */
	public function testGetInterestRate() {
		$calc = $this->getCalc();
		$date = new DateTime('1980-01-01');
		$calc->setDueDate($date);
		$this->assertSame(25.0, $calc->getInterestRate());
	}
	
	/**
	 * @covers ::getDailyInterest
	 * @depends testGetInterestRate
	 */
	public function testGetDailyInterest() {
		$calc = $this->getCalc();
		$date = new DateTime('1980-01-01');
		$calc->setDueDate($date)->setInvoiceAmount(100);
		$rate = $calc->getDailyInterest();
		$this->assertInternalType('float', $rate);
		$rounded = round($rate, 3, PHP_ROUND_HALF_UP);
		$this->assertSame(.068, $rounded);
	}
	
	/**
	 * @covers ::getDailyInterest
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testGetDailyInterestCausesExceptionWithoutDatesSet() {
		$calc = $this->getCalc();
		$calc->setInvoiceAmount(100);
		$calc->getDailyInterest();
	}
	
	/**
	 * @covers ::getDailyInterest
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testGetDailyInterestCausesExceptionWithoutInvoiceAmountSet() {
		$calc = $this->getCalc();
		$date = new DateTime('1980-01-01');
		$calc->setDueDate($date);
		$rate = $calc->getDailyInterest();
	}
	
	/**
	 * @covers ::getInterest
	 * @depends testGetDailyInterest
	 */
	public function testGetInterest() {
		$calc = $this->getCalc();
		$due = new DateTime('-10 days');
		$calc->setDueDate($due)->setTerms(0)->setInvoiceAmount(100);
		$daily = $calc->getDailyInterest();
		// Total interest is rounded
		$expect = round( (10 * $daily), 2, PHP_ROUND_HALF_UP);
		$amount = $calc->getInterest();
		$this->assertInternalType('float', $amount);
		$this->assertSame($expect, $amount);
	}
	
	/**
	 * @covers ::getFee
	 */
	public function testGetFeeReturnsZeroForNotOverdue() {
		$calc = $this->getCalc();
		$calc->setDueDate('now'); // Zero Days Overdue
		$fee = $calc->getFee();
		$this->assertInternalType('float', $fee);
		$this->assertSame(0.0, $fee);
	}
	
	/**
	 * @covers ::getFee
	 */
	public function testGetFeeReturnsZeroForUnsetInvoiceAmount() {
		$calc = $this->getCalc();
		$calc->setDueDate('-10 days');
		$fee = $calc->getFee();
		$this->assertInternalType('float', $fee);
		$this->assertSame(0.0, $fee);
	}
	
	/**
	 * @covers ::getFee
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testGetFeeCausesExceptionWithNullDates() {
		$calc = $this->getCalc();
		$fee = $calc->getFee();
	}
	
	/**
	 * @covers ::getFee
	 */
	public function testGetFee() {
		
		$data = array(
			array(10, 40.0),
			array(1000, 70.0),
			array(10000, 100.0),
			array(0.01, 40.0),
			array(999.99, 40.0),
			array(0, 0.0),
			array(9999.99, 70.0),
			array(9999999, 100.0),
		);
		
		foreach($data as $p) {
			$calc = $this->getCalc();
			$calc->setDueDate('-10 days');
			$calc->setInvoiceAmount($p[0]);
			$fee = $calc->getFee();
			$this->assertInternalType('float', $fee);
			$this->assertSame($p[1], $fee);
		}
		
	}
	
	/**
	 * @covers ::getGrandTotal
	 */
	public function testGetGrandTotal() {
		$calc = $this->getCalc();
		$calc->setDueDate('-10 days');
		$calc->setInvoiceAmount(100);
		$fee = $calc->getFee();
		$interest = $calc->getInterest();
		$expect = 100 + $fee + $interest;
		$this->assertSame($expect, $calc->getGrandTotal());
	}
	
	/**
	 * @covers ::setDateFormat
	 * @covers ::getDateFormat
	 */
	public function testSetGetDateFormat() {
		$calc = $this->getCalc();
		// Initially set
		$fmt = $calc->getDateFormat();
		$this->assertInternalType('string', $fmt);
		$this->assertTrue( (strlen($fmt) > 1) );
		// Fluid Setter
		$this->assertSame($calc, $calc->setDateFormat('foo'));
		$this->assertSame('foo', $calc->getDateFormat());
	}
	
	/**
	 * @covers ::getArrayCopy
	 */
	public function testGetArrayCopy() {
		$calc = $this->getCalc();
		$inv = new DateTime('-1 day');
		$calc->setInvoiceDate($inv)
			->setTerms(0)
			->setInvoiceAmount(100)
			->setDateFormat('Y-m-d');
		
		$expect = array(
			'invoiceDate' => $inv->format('Y-m-d'),
			'daysOverdue' => 1,
			'dueDate' => $inv->format('Y-m-d'),
			'terms' => 0,
			'invoiceAmount' => 100.0,
			'statutoryInterestRate' => $calc->getStatutoryInterestRate(),
			'referenceRate' => $calc->getReferenceRate(),
			'fee' => 40.0,
			'interest' => $calc->getInterest(),
			'dailyInterest' => $calc->getDailyInterest(),
			'grandTotal' => $calc->getGrandTotal(),
		);
		
		$data = $calc->getArrayCopy();
		$this->assertInternalType('array', $data);
		$this->assertCount(count($expect), $data);
		foreach($expect as $param => $val) {
			$this->assertSame($val, $data[$param]);
		}
	}
	
	/**
	 * @covers ::getInputFilter
	 */
	public function testGetInputFilterBasic() {
		$calc = $this->getCalc();
		$this->assertInstanceOf('Zend\InputFilter\InputFilterInterface', $calc->getInputFilter());
	}
}