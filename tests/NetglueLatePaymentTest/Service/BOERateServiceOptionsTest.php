<?php

namespace NetglueLatePaymentTest\Service;
use PHPUnit_Framework_TestCase;

use NetglueLatePayment\Service\BOERateServiceOptions as ServiceOptions;
use DateTime;
use DateTimeZone;


/**
 * @coversDefaultClass NetglueLatePayment\Service\BOERateServiceOptions
 */
class BOERateServiceOptionsTest extends PHPUnit_Framework_TestCase {
	
	public function tearDown() {
		$dir = '/tmp/ng-test-nowrite';
		if(is_dir($dir)) {
			rmdir($dir);
		}
	}
	
	/**
	 * @covers ::getStorageDirectory
	 */
	public function testGetStorageDirectoryHasDefaultValue() {
		$opt = new ServiceOptions;
		$this->assertInternalType('string', $opt->getStorageDirectory());
		$this->assertTrue(strlen($opt->getStorageDirectory()) > 1);
	}
	
	/**
	 * @covers ::setStorageDirectory
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testSetStorageDirThrowsExceptionIfNotDir() {
		$opt = new ServiceOptions;
		$opt->setStorageDirectory(__FILE__);
	}
	
	/**
	 * @covers ::setStorageDirectory
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testSetStorageDirThrowsExceptionIfNotWritable() {
		$opt = new ServiceOptions;
		$dir = '/tmp/ng-test-nowrite';
		mkdir($dir);
		chmod($dir, 0555);
		$this->assertFalse(is_writable($dir));
		$opt->setStorageDirectory($dir);
	}
	
	/**
	 * @covers ::setStorageDirectory
	 */
	public function testSetStorageDir() {
		$opt = new ServiceOptions;
		$dir = '/tmp/ng-test-write';
		mkdir($dir);
		$opt->setStorageDirectory($dir);
		$this->assertSame($dir, $opt->getStorageDirectory());
		rmdir($dir);
	}
	
	/**
	 * @covers ::getRateFilename
	 */
	public function testGetRateFilenameHasDefaultValue() {
		$opt = new ServiceOptions;
		$this->assertInternalType('string', $opt->getRateFilename());
		$this->assertTrue(strlen($opt->getRateFilename()) > 1);
		$this->assertRegExp('/\.json$/', $opt->getRateFileName());
	}
	
	/**
	 * @covers ::setRateFilename
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testSetRateFileThrowsExceptionIfNotString() {
		$opt = new ServiceOptions;
		$opt->setRateFilename(array());
	}
	
	/**
	 * @covers ::setRateFilename
	 */
	public function testSetRateFilename() {
		$opt = new ServiceOptions;
		$opt->setRateFilename('foo.json');
		$this->assertSame('foo.json', $opt->getRateFilename());
	}
}