<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace NetglueLatePaymentTest\Service;
use PHPUnit_Framework_TestCase;

use NetglueLatePayment\Service\BOERateClient as RateClient;
use DateTime;
use DateTimeZone;


/**
 * @coversDefaultClass NetglueLatePayment\Service\BOERateClient
 */
class BOERateClientTest extends PHPUnit_Framework_TestCase {
	
	
	/**
	 * @covers ::getHttpClient
	 */
	public function testGetHttpClient() {
		$client = new RateClient;
		$http = $client->getHttpClient();
		$this->assertInstanceOf('Zend\Http\Client', $http);
		$uri = (string) $http->getUri();
		$this->assertStringStartsWith('http://www.bankofengland.co.uk', $uri);
		// Same instance
		$http2 = $client->getHttpClient();
		$this->assertSame($http, $http2);
	}
	
	/**
	 * @covers ::__construct
	 * @covers ::getTimezone
	 */
	public function testConstruct() {
		$me = new RateClient;
		$this->assertInstanceOf('DateTimeZone', $me->getTimezone());
		
		// Timezone Should Be 'Europe/London'
		$tz = $me->getTimezone();
		$this->assertSame('Europe/London', $tz->getName());
	}
	
	/**
	 * @covers ::getRateFile
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testNoContentTypeXmlThrowsException() {
		$me = new RateClient;
		$http = $me->getHttpClient();
		$http->setUri('http://www.google.com/');
		$me->findAllRates();
	}
	
	/**
	 * @covers ::getUrlParams
	 */
	public function testGetUrlParams() {
		$me = new RateClient;
		
		// Without any dates, we should be asing for all data
		$params = $me->getUrlParams();
		$this->assertInternalType('array', $params);
		$this->assertArrayHasKey('DAT', $params);
		$this->assertSame('ALL', $params['DAT']);
		
		// Given a from date and NULL to date. To should get set to the current date
		$from = DateTime::createFromFormat('j/n/Y', '1/9/2013');
		$me = new RateClient;
		$params = $me->getUrlParams($from);
		$this->assertInternalType('array', $params);
		$this->assertArrayHasKey('TD', $params);
		$this->assertArrayHasKey('TM', $params);
		$this->assertArrayHasKey('TY', $params);
		$this->assertSame(date('j'), $params['TD']);
		$this->assertSame(date('M'), $params['TM']);
		$this->assertSame(date('Y'), $params['TY']);
	}
	
	/**
	 * @covers ::getUrlParams
	 * @expectedException NetglueLatePayment\Exception\ExceptionInterface
	 */
	public function testGetUrlParamsThrowsExceptionForInvalidDateRange() {
		$from = DateTime::createFromFormat('j-n-Y', '1-1-2012');
		$to = DateTime::createFromFormat('j-n-Y', '1-12-2011');
		$me = new RateClient;
		$params = $me->getUrlParams($from, $to);
	}
	
	/**
	 * @covers ::findRatesBetween
	 * @covers ::ratesToArray
	 * @covers ::getRateFile
	 */
	public function testFindDateRangeBasic() {
		$client = new RateClient;
		$time = strtotime("-1 week");
		$date = new DateTime(NULL, $client->getTimezone());
		$date->setTimestamp($time);

		$rates = $client->findRatesBetween($date);
		$this->assertInternalType('array', $rates);
		$this->assertArrayHasKey('info', $rates);
		$this->assertInternalType('array', $rates['info']);
		$this->assertArrayHasKey('firstObservation', $rates['info']);
		$this->assertArrayHasKey('lastObservation', $rates['info']);
		foreach($rates as $key => $line) {
			if($key === 'info') {
				continue;
			}
			// Array key should be Ymd
			$this->assertRegExp('/[0-9]{6}/', (string) $key);
			$this->assertInternalType('array', $line);
			$this->assertArrayHasKey('date', $line);
			$this->assertArrayHasKey('rate', $line);
			$this->assertTrue(is_numeric($line['rate']));
		}
		unset($rates['info']);
		reset($rates);
		$first = current($rates);
		$expect = (int) $date->format("Ymd");
		$actual = (int) $first['dateTime']->format("Ymd");
		
		$this->assertGreaterThanOrEqual($expect, $actual);
	}
	
	/**
	 * @covers ::findRatesBetween
	 * @covers ::ratesToArray
	 * @covers ::getRateFile
	 */
	public function testFindRatesBetweenAcceptsStrings() {
		$client = new RateClient;
		$rates = $client->findRatesBetween('1st September 2013', '15th September 2013');
		unset($rates['info']);
		reset($rates);
		$first = current($rates);
		$last = end($rates);
		
		$expect = 20130901;
		$actual = (int) $first['dateTime']->format('Ymd');
		$this->assertGreaterThanOrEqual($expect, $actual);
		
		$expect = 20130915;
		$actual = (int) $last['dateTime']->format('Ymd');
		$this->assertLessThanOrEqual($expect, $actual);
	}
	
	/**
	 * @covers ::findAllRates
	 * @covers ::ratesToArray
	 * @covers ::getRateFile
	 */
	public function testFindAllRatesBasic() {
		$client = new RateClient;
		$rates = $client->findAllRates();
		$this->assertInternalType('array', $rates);
		$this->assertArrayHasKey('info', $rates);
		$this->assertInternalType('array', $rates['info']);
		$this->assertArrayHasKey('firstObservation', $rates['info']);
		$this->assertArrayHasKey('lastObservation', $rates['info']);
		foreach($rates as $key => $line) {
			if($key === 'info') {
				continue;
			}
			// Array key should be Ymd
			$this->assertRegExp('/[0-9]{6}/', (string) $key);
			$this->assertInternalType('array', $line);
			$this->assertArrayHasKey('date', $line);
			$this->assertArrayHasKey('rate', $line);
			$this->assertTrue(is_numeric($line['rate']));
		}
	}
}