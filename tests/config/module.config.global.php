<?php

return array(
    'view_manager' => array(
		'display_not_found_reason' => true,
        'display_exceptions'       => true,
		'template_map' => array(
			'error' => __DIR__ . '/../view/blank.phtml',
			'layout/layout' => __DIR__ . '/../view/blank.phtml',
		),
	),
);
